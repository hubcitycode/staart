FROM node:10.16.0-alpine
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY ./package.json ./
RUN npm install && npm cache clean --force
COPY . .

RUN ["npm", "run", "build"]
